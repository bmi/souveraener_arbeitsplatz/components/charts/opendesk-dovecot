## [1.4.2](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/compare/v1.4.1...v1.4.2) (2025-03-10)


### Bug Fixes

* **dovecot:** Add recipient delimiter support ([22b4588](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/commit/22b458819b034f18b820c26b3c86e5f87e764d23))

## [1.4.1](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/compare/v1.4.0...v1.4.1) (2024-11-22)


### Bug Fixes

* **ldap-userdn.conf.ext:** Add `oxresources/oxresources` to `user_filter`. ([d62faea](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/commit/d62faea103f22d29b302265a006aae3b676a4399))

# [1.4.0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/compare/v1.3.10...v1.4.0) (2024-09-23)


### Features

* Add option for (temporary) masterpassword authentication for migrations. ([3b0f1d0](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/commit/3b0f1d0432d71ccdd4505f39f5d23ccf481cae1d))

## [1.3.10](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/compare/v1.3.9...v1.3.10) (2024-04-11)


### Bug Fixes

* **dovecot:** Only provide storageClassName when provided ([4dcce3c](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/commit/4dcce3c7f26c55dc434ffc82fa6d9e2ee97cd570))

## [1.3.9](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/compare/v1.3.8...v1.3.9) (2023-12-21)


### Bug Fixes

* Add GPG key, update README.md ([6a65f22](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/commit/6a65f223bf5842955f33636fe8938ecb947efcf5))

## [1.3.8](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/compare/v1.3.7...v1.3.8) (2023-12-20)


### Bug Fixes

* **ci:** Move repo to Open CoDE ([e04ce09](https://gitlab.opencode.de/bmi/opendesk/components/platform-development/charts/opendesk-dovecot/commit/e04ce09d52ce95bd1b69480e807c45bd8973bde0))

## [1.3.7](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.3.6...v1.3.7) (2023-12-15)


### Bug Fixes

* **oidc:** Adapt to Univention Keycloak deployment ([dacd288](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/dacd288609e415f586996692dd83ed75b74e3e31))

## [1.3.6](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.3.5...v1.3.6) (2023-11-22)


### Bug Fixes

* **dovecot:** Add kill capability ([e0d773d](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/e0d773d0047318a94f0726a6cd1432f5cfc87e2f))

## [1.3.5](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.3.4...v1.3.5) (2023-11-14)


### Bug Fixes

* **dovecot:** Add authentication to introspection URL ([9d8ee64](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/9d8ee6449cdd4a77ddd121246ebf002ca9c30bfa))
* **dovecot:** Add autoresponder ([bf076c0](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/bf076c0c6d9fc4bc0c185d2cd761784813e8eeaf))

## [1.3.4](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.3.3...v1.3.4) (2023-10-20)


### Bug Fixes

* **dovecot:** Replace AsConfig as it cannot handle functions inside template ([68712ff](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/68712ffc4f9e03b2d7fc0d4264b64e6a40ff7826))

## [1.3.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.3.2...v1.3.3) (2023-10-17)


### Bug Fixes

* **dovecot:** Use "| quote" wherever possible ([d2eda6e](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/d2eda6e54055faf46568d3b6ff4f0dbfad90f127))

## [1.3.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.3.1...v1.3.2) (2023-10-16)


### Bug Fixes

* **dovecot:** Use "| quote" wherever possible ([0b73fb2](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/0b73fb22a57ac9bcfd9ebf6dc47cb3e59b7d2172))

## [1.3.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.3.0...v1.3.1) (2023-08-11)


### Bug Fixes

* Workaround for latest change in OX-Connector when provisioning functional mailboxes ([13caf7e](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/13caf7e09225d18cf781653a2e8957cc3ee29853))

# [1.3.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.2.0...v1.3.0) (2023-08-10)


### Features

* **dovecot:** Add support for image digest ([19a70ac](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/19a70acec34da0fb800c7e9f7c2842c1b0d4913d))

# [1.2.0](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/compare/v1.1.0...v1.2.0) (2023-08-03)


### Features

* add configurable servicetype ([4577bf5](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/4577bf540243509e2e69a783a80da1cf4990a0d0))
* Update to new chart structure ([c12e8c2](https://gitlab.souvap-univention.de/souvap/tooling/charts/dovecot/commit/c12e8c2f53618bce6a1dac5d68686d89badbf9b3))
