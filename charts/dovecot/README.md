<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
# dovecot

A Helm chart for running Dovecot for OX

## Installing the Chart

To install the chart with the release name `my-release`:

```console
helm repo add opendesk-dovecot https://gitlab.opencode.de/api/v4/projects/1384/packages/helm/stable
helm install my-release opendesk-dovecot/dovecot
```

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | common | ^2.x.x |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity Note: podAffinityPreset, podAntiAffinityPreset, and  nodeAffinityPreset will be ignored when it's set |
| autoscaling.enabled | bool | `false` | Enable Horizontal Pod Autoscaling |
| autoscaling.maxReplicas | int | `100` | Maximum amount of Replicas |
| autoscaling.minReplicas | int | `1` | Minimum amount of Replicas |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | set the CPU Utilization Percentage |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` | set the Memory Utilization Percentage |
| certificate.secretName | string | `""` | mandatory secret name of the TLS certificate to use will not be created by this helm chart! |
| cleanup.keepExternalServiceOnDelete | bool | `true` | Keep external Service on delete (to keep LoadBalancer IP) |
| cleanup.keepPVCOnDelete | bool | `false` | Keep persistence on delete of this release. |
| commonAnnotations | object | `{}` | Additional custom annotations to add to all deployed objects. |
| commonLabels | object | `{}` | Additional custom labels to add to all deployed objects. |
| containerSecurityContext.allowPrivilegeEscalation | bool | `false` | Enable container privileged escalation. |
| containerSecurityContext.capabilities | object | `{"add":["CHOWN","DAC_OVERRIDE","KILL","NET_BIND_SERVICE","SETGID","SETUID","SYS_CHROOT"],"drop":["ALL"]}` | drop or add capabilites here https://kubernetes.io/docs/concepts/security/pod-security-standards/ |
| containerSecurityContext.enabled | bool | `true` | Enable security context. |
| containerSecurityContext.readOnlyRootFilesystem | bool | `true` | Mounts the container's root filesystem as read-only. |
| containerSecurityContext.seccompProfile | object | `{"type":"RuntimeDefault"}` | Set a seccomp profile. |
| dovecot.ldap.base | string | `""` | LDAP Base |
| dovecot.ldap.dn | string | `""` | LDAP DN |
| dovecot.ldap.enabled | bool | `true` | enable LDAP |
| dovecot.ldap.host | string | `""` | LDAP Host |
| dovecot.ldap.password | string | `""` | LDAP Password |
| dovecot.ldap.port | int | `389` | LDAP Port |
| dovecot.loginTrustedNetworks | string | `""` | https://doc.dovecot.org/settings/core/#core_setting-login_trusted_networks |
| dovecot.mailDomain | string | `""` | should be a FQDN, only used for the greeter |
| dovecot.migration.enabled | bool | `false` | Enable temporary for master password authentication in case of migrations |
| dovecot.migration.masterPassword | string | `""` | Set the master password for the migration |
| dovecot.oidc.clientID | string | `""` | OIDC Client ID |
| dovecot.oidc.clientSecret | string | `""` | OIDC Client Secret |
| dovecot.oidc.enabled | bool | `true` | enable OIDC |
| dovecot.oidc.introspectionHost | string | `""` | OIDC introspection host |
| dovecot.oidc.introspectionPath | string | `""` | OIDC introspection path |
| dovecot.oidc.introspectionScheme | string | `"https"` | OIDC introspection scheme |
| dovecot.oidc.usernameAttribute | string | `""` | OIDC Username Attribute |
| dovecot.password | string | `""` | doveadm password |
| dovecot.recipientDelimiter | string | `"+"` |  |
| dovecot.submission.enabled | bool | `true` | enable submitting messages through external relay |
| dovecot.submission.host | string | `"postfix:25"` | submission host and port, specify as "host:port" |
| dovecot.submission.ssl | string | `"no"` | whether to use smtps or starttls or no encryption ("no") |
| extraEnvVars | list | `[]` | Array with extra environment variables to add to containers.  extraEnvVars:   - name: FOO     value: "bar"  |
| extraVolumeMounts | list | `[]` | Optionally specify extra list of additional volumeMounts. |
| extraVolumes | list | `[]` | Optionally specify extra list of additional volumes |
| fullnameOverride | string | `""` | Provide a name to substitute for the full names of resources. |
| global.imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| global.registry | string | `"docker.io"` | Container registry address. |
| image.imagePullPolicy | string | `"IfNotPresent"` | Define an ImagePullPolicy.  Ref.: https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy  "IfNotPresent" => The image is pulled only if it is not already present locally. "Always" => Every time the kubelet launches a container, the kubelet queries the container image registry to             resolve the name to an image digest. If the kubelet has a container image with that exact digest cached             locally, the kubelet uses its cached image; otherwise, the kubelet pulls the image with the resolved             digest, and uses that image to launch the container. "Never" => The kubelet does not try fetching the image. If the image is somehow already present locally, the            kubelet attempts to start the container; otherwise, startup fails  |
| image.registry | string | `"external-registry.souvap-univention.de/sovereign-workplace"` | Container registry address. This setting has higher precedence than global.registry. registry: "docker.io" |
| image.repository | string | `"souvap/tooling/images/dovecot-public-sector"` | Container repository string. repository: "dovecot/dovecot" |
| image.tag | string | `"2.3.21@sha256:c76965a84d1ca527f523404eb027119f6736b199c094e4671037cb345ecad3dc"` | Set the image tag and the digest tag: "2.3.21@sha256:1c18c756f20d03867077a1b509a6e2e3008ab1eafa56377b6f2eca12dc1ba581" |
| imagePullSecrets | list | `[]` | Credentials to fetch images from private registry Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/  imagePullSecrets:   - "docker-registry"  |
| lifecycleHooks | object | `{}` | Lifecycle to automate configuration before or after startup |
| livenessProbe.enabled | bool | `true` | Enables kubernetes LivenessProbe. |
| livenessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| livenessProbe.initialDelaySeconds | int | `15` | Delay after container start until LivenessProbe is executed. |
| livenessProbe.periodSeconds | int | `20` | Time between probe executions. |
| livenessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| livenessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| mailDomains | string | `""` |  |
| nameOverride | string | `""` | String to partially override release name. |
| nodeSelector | object | `{}` | Node labels for pod assignment Ref: https://kubernetes.io/docs/user-guide/node-selection/ |
| persistence.accessModes | list | `["ReadWriteOnce"]` | The volume access modes, some of "ReadWriteOnce", "ReadOnlyMany", "ReadWriteMany", "ReadWriteOncePod".  "ReadWriteOnce" => The volume can be mounted as read-write by a single node. ReadWriteOnce access mode still can                    allow multiple pods to access the volume when the pods are running on the same node. "ReadOnlyMany" => The volume can be mounted as read-only by many nodes. "ReadWriteMany" => The volume can be mounted as read-write by many nodes. "ReadWriteOncePod" => The volume can be mounted as read-write by a single Pod. Use ReadWriteOncePod access mode if                       you want to ensure that only one pod across whole cluster can read that PVC or write to it.  |
| persistence.annotations | object | `{}` | Annotations for the PVC. |
| persistence.dataSource | object | `{}` | Custom PVC data source. |
| persistence.enabled | bool | `true` | Enable data persistence (true) or use temporary storage (false). |
| persistence.labels | object | `{}` | Labels for the PVC. |
| persistence.selector | object | `{}` | Selector to match an existing Persistent Volume (this value is evaluated as a template)  selector:   matchLabels:     app: my-app  |
| persistence.size | string | `"1Gi"` |  |
| persistence.storageClassName | string | `""` | The (storage) class of PV. |
| podAnnotations | object | `{}` | Pod Annotations. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/ |
| podLabels | object | `{}` | Pod Labels. Ref: https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/ |
| podSecurityContext.enabled | bool | `true` | if securityContext on pod level is enabled |
| podSecurityContext.fsGroup | int | `1000` | set volume mounts group permission to vmail! default is 1000 |
| readinessProbe.enabled | bool | `true` | Enables kubernetes ReadinessProbe. |
| readinessProbe.failureThreshold | int | `10` | Number of failed executions until container is terminated. |
| readinessProbe.initialDelaySeconds | int | `15` | Delay after container start until ReadinessProbe is executed. |
| readinessProbe.periodSeconds | int | `20` | Time between probe executions. |
| readinessProbe.successThreshold | int | `1` | Number of successful executions after failed ones until container is marked healthy. |
| readinessProbe.timeoutSeconds | int | `5` | Timeout for command return. |
| replicaCount | int | `1` | Set the amount of replicas of deployment. |
| resources.limits.cpu | float | `0.5` | The max amount of CPUs to consume. |
| resources.limits.memory | string | `"256Mi"` | The max amount of RAM to consume. |
| resources.requests.cpu | float | `0.1` | The amount of CPUs which has to be available on the scheduled node. |
| resources.requests.memory | string | `"128Mi"` | The amount of RAM which has to be available on the scheduled node. |
| service.annotations | object | `{}` | Additional custom annotations |
| service.enabled | bool | `true` | Enable kubernetes service creation. |
| service.external.enabled | bool | `false` | Wether dovecot shall be reachable publicly |
| service.external.ports | object | `{"imaps":{"port":993},"pop3s":{"port":995}}` | if the type is LoadBalancer, the specified ports will be exposed e.g. imaps:   port: 993 If the type is NodePoart, exposed Ports and the corresponding nodePorts need to be defined imaps:   port: 993   nodePort: 30993 Protocol will default to TCP |
| service.external.type | string | `"LoadBalancer"` | How to expose dovecot, choices can be LoadBalancer or NodePort |
| service.ports | object | `{"auth":{"port":3659},"doveadm":{"port":9000},"doveadm2":{"port":9200},"doveadmhttp":{"port":8080},"imap":{"port":143},"imaps":{"port":993},"lmpt":{"port":24},"managasieve":{"port":4190},"openmetrics":{"port":9900},"pop3":{"port":110},"pop3s":{"port":995}}` | All ports to expose internally via the dovecot service: e.g. imaps:   port: 993 |
| service.sessionAffinity.enabled | bool | `false` | Wether session affinity should be enabled |
| serviceAccount.annotations | object | `{}` | Additional custom annotations for the ServiceAccount. |
| serviceAccount.automountServiceAccountToken | bool | `true` | Allows auto mount of ServiceAccountToken on the serviceAccount created. Can be set to false if pods using this serviceAccount do not need to use K8s API. |
| serviceAccount.create | bool | `true` | Enable creation of ServiceAccount for pod. |
| serviceAccount.labels | object | `{}` | Additional custom labels for the ServiceAccount. |
| terminationGracePeriodSeconds | string | `""` | In seconds, time the given to the pod needs to terminate gracefully. Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod/#termination-of-pods |
| tolerations | list | `[]` | Tolerations for pod assignment Ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/ |
| topologySpreadConstraints | list | `[]` | Topology spread constraints rely on node labels to identify the topology domain(s) that each Node is in Ref: https://kubernetes.io/docs/concepts/workloads/pods/pod-topology-spread-constraints/  topologySpreadConstraints:   - maxSkew: 1     topologyKey: failure-domain.beta.kubernetes.io/zone     whenUnsatisfiable: DoNotSchedule |
| updateStrategy.type | string | `"Recreate"` | Set to Recreate if you use persistent volume that cannot be mounted by more than one pods to make sure the pods is destroyed first. |

## Making Dovecot available to the Internet

In order to connect to Dovecot using a MUA like Thunderbird the Ports 993 for IMAPS or 995 for POP3S need to be reachable on a public IP.
Currently, there are 2 options supported: `LoadBalanacer` and `NodePort`. Depending on your cluster/network setup, there might be also other options which are not described in this readme.

### Option 1: service of type `LoadBalancer`

This requires a cloud setup that enables a Loadbalancer attachement. This could be enabled via values:

```yaml
service:
  external:
    enabled: true
    type: "LoadBalancer"
    ports:
      imaps:
        port: 993
      pop3s:
        port: 995
```

### Option 2: NodePort with NAT (Network Address Translation) and PAT (Port Address Translation)

This setup requires an external firewall or loadbalancer with a public IP and port translation from the standard ports (993 for imaps and 995 for pop3s) on the external IP to the corresponding NodePorts on the internal Node IPs.

```yaml
service:
  external:
    enabled: true
    type: "NodePort"
    ports:
      imaps:
        port: 993
        nodePort: 30993
      pop3s:
        port: 995
        nodePort: 30995
```

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
