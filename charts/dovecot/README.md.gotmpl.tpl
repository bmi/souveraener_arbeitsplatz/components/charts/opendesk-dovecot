<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"

SPDX-License-Identifier: Apache-2.0
-->
{{ template "chart.header" . }}
{{ template "chart.description" . }}

## Installing the Chart

To install the chart with the release name `my-release`:

```console
helm repo add ${CI_PROJECT_NAME} ${CI_SERVER_PROTOCOL}://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/helm/stable
helm install my-release ${CI_PROJECT_NAME}/{{ template "chart.name" . }}
```

{{ template "chart.requirementsSection" . }}

{{ template "chart.valuesSection" . }}

## Making Dovecot available to the Internet

In order to connect to Dovecot using a MUA like Thunderbird the Ports 993 for IMAPS or 995 for POP3S need to be reachable on a public IP.
Currently, there are 2 options supported: `LoadBalanacer` and `NodePort`. Depending on your cluster/network setup, there might be also other options which are not described in this readme.

### Option 1: service of type `LoadBalancer`

This requires a cloud setup that enables a Loadbalancer attachement. This could be enabled via values:

```yaml
service:
  external:
    enabled: true
    type: "LoadBalancer"
    ports:
      imaps:
        port: 993
      pop3s:
        port: 995
```

### Option 2: NodePort with NAT (Network Address Translation) and PAT (Port Address Translation)

This setup requires an external firewall or loadbalancer with a public IP and port translation from the standard ports (993 for imaps and 995 for pop3s) on the external IP to the corresponding NodePorts on the internal Node IPs.

```yaml
service:
  external:
    enabled: true
    type: "NodePort"
    ports:
      imaps:
        port: 993
        nodePort: 30993
      pop3s:
        port: 995
        nodePort: 30995
```

## Uninstalling the Chart

To install the release with name `my-release`:

```bash
helm uninstall my-release
```

## Signing

Helm charts are signed with helm native signing method.

You can verify the chart against [the public GPG key](../../files/gpg-pubkeys/opendesk.gpg).

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright (C) 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
