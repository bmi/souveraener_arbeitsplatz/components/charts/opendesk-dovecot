{{- /*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
#!/bin/sh

PERCENT=\$1
USER=\$2
cat << EOF | /usr/lib/dovecot/dovecot-lda -d \$USER -o "plugin/quota=count:User quota:noenforcing"
From: postmaster@{{ .Values.dovecot.mailDomain }}
To: \$USER
Date: \$(date +"%a, %d %b %Y %H:%M:%S %z")
Subject: Quota Warnung zu Ihrem E-Mail Konto
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

Guten Tag,

Ihr Postkorb \$USER ist aktuell zu \$PERCENT% gefüllt.

Bitte archivieren oder löschen Sie alte, nicht mehr benötige Nachrichten. Andernfalls
könnte es vorkommen, dass Sie keine neuen Nachrichten mehr empfangen können.

Mit freundlichem Gruß,
 Ihr Postmaster
EOF
